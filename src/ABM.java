import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ABM extends JFrame implements ActionListener {
	
	JButton boton1;
	JTextField text1, text2, text3, text4, text5, text6;
	String nombre, apellido, mail;
	double monto;
	int dni, nroCuenta;
	
	public ABM() {
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 25, 25));
		
		panel.add(new JLabel("Nombre"));
		panel.add(text1 = new JTextField(5));
		panel.add(new JLabel("Apellido"));
		panel.add(text2 = new JTextField(5));
		panel.add(new JLabel("Mail"));
		panel.add(text3 = new JTextField(5));
		panel.add(new JLabel("Monto"));
		panel.add(text4 = new JTextField(5));
		panel.add(new JLabel("DNI"));
		panel.add(text5 = new JTextField(5));
		panel.add(new JLabel("N� Cuenta"));
		panel.add(text6 = new JTextField(5));
		
		panel.add(boton1 = new JButton("Aceptar"));
		
		add(panel);
		
		boton1.addActionListener(this);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ABM formulario = new ABM();		
		formulario.setSize(335, 300);
		formulario.setTitle("ABM Clientes");
		formulario.setDefaultCloseOperation(EXIT_ON_CLOSE);
		formulario.setVisible(true);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		// System.out.println("Test");
		if (text1.getText().isBlank() || text2.getText().isBlank() || text3.getText().isBlank() || text4.getText().isBlank() || text5.getText().isBlank() || text6.getText().isBlank()) {
		JOptionPane.showMessageDialog(null,"ERROR: Debe completar todos los campos.");
		}else {
		nombre = text1.getText();
		text1.setText("");
		apellido = text2.getText();
		text2.setText("");
		mail = text3.getText();
		text3.setText("");
		
		monto = Double.parseDouble(text4.getText());
		text4.setText("");
		
		dni = Integer.parseInt(text5.getText());		
		text5.setText("");		
		nroCuenta = Integer.parseInt(text6.getText());		
		text6.setText("");
		

		Cliente cli1 = new Cliente(nombre, apellido, mail, monto, dni, nroCuenta);

		BaseDeDatos bd = new BaseDeDatos();
		
		try {
			bd.agregarCliente(cli1);
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		JOptionPane.showMessageDialog(null,"El cliente se ha cargado correctamente.");
		}
	}

}
